.. :changelog:

=========
Changelog
=========

0.2.0 (2016-08-08)
++++++++++++++++++

* Introduced different communication protocols (XML RPC and JSON RPC)

0.1.2 (2016-07-13)
++++++++++++++++++

* Fixed broken setup.py.

0.1.1 (2016-07-13)
++++++++++++++++++

* Added documentation.

0.1.0 (2016-07-13)
++++++++++++++++++

* Initial release to pypi.
