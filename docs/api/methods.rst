================
Method Reference
================

Includes all functions provided by the XML RPC API, split into logical entities.

System methods
==============

.. autoclass:: gearthonic.methods.SystemMethodsCollection
    :members:

General methods
===============

.. autoclass:: gearthonic.methods.GeneralMethodsCollection
    :members:

Device methods
==============

.. autoclass:: gearthonic.methods.DeviceMethodsCollection
    :members:

Pairing methods
===============

.. autoclass:: gearthonic.methods.PairingMethodsCollection
    :members:

Family methods
==============

.. autoclass:: gearthonic.methods.FamilyMethodsCollection
    :members:

Event Server methods
====================

.. autoclass:: gearthonic.methods.EventServerMethodsCollection
    :members:

Physical Interface methods
==========================

.. autoclass:: gearthonic.methods.PhysicalInterfaceMethodsCollection
    :members:

Metadata methods
================

.. autoclass:: gearthonic.methods.MetadataMethodsCollection
    :members:

System variables methods
========================

.. autoclass:: gearthonic.methods.SystemVariableMethodsCollection
    :members:
