.. complexity documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

Contents:
=========

.. toctree::
    :maxdepth: 2

    usage
    api/index
    contributing
    changelog

Feedback
========

If you have any suggestions or questions about **gearthonic** feel free to email me
at mumpitz@wumpitz.de.

If you encounter any errors or problems with **gearthonic**, please let me know!
Open an Issue at the Gitlab http://gitlab.com/wumpitz/gearthonic main repository.
