=====
Usage
=====

Basic usage
===========

To use Gearthonic in a project::

	from gearthonic import GearClient
	# Initialise the client with the host and port of your homegear server
	gc = GearClient('192.168.1.100', 2003)
	# Now you can make your requests
	gc.system.list_methods()
	gc.device.get_value(1, 4, 'ACTUAL_TEMPERATURE')


All methods are already implemented within the client to make it easy to use. You don't have to lookup all methods and
their parameters, just look at the code or see :doc:`api/methods`.

Alternatively you can call any method directly via the client or use the generic ''call''-method::

    gc.getValue(1, 4, 'ACTUAL_TEMPERATURE')
    gc.call('system.listDevices')
    gc.call('getValue', 1, 4, 'ACTUAL_TEMPERATURE')

A full list of all available methods provided by the Homegear API can be found in the `wiki of the Homegear project`_.

Communication protocols
=======================

Gearthonic supports different communication protocols to communicate with your Homegear server. Set the protocol while
initialising the client::

    from gearthonic import JSONRPC
    gc = GearClient('192.168.1.100', 2003, protocol=JSONRPC)

The default protocol is ``XMLRPC``.

Any protocol can accept additional parameters. You can supply them while initialising the client::

    gc = GearClient('192.168.1.100', 2003, secure=False, verify=False)

For a complete list of available protocols and supported parameters look at :mod:`gearthonic.protocols`.

Security
========

XML RPC and JSON RPC
--------------------

If you set ``secure=True`` while initialising the GearClient, the client tries to establish a secure connection to the
server via SSL. It's highly recommended to use SSL for the network traffic. Otherwise the communication is not
encrypted.

If you are using a self signed certificate, you can skip the verification of the certificate. Set ``verify=False`` while
initialising the GearClient and the certificate won't be verified. But keep in mind: that's not suggested!

Additional information
======================

* `Documentation of all available data endpoints for all devices`_
* `wiki of the Homegear project`_


.. _wiki of the Homegear project: https://www.homegear.eu/index.php/XML_RPC_Method_Reference
.. _Documentation of all available data endpoints for all devices: http://www.eq-3.de/Downloads/eq3/download%20bereich/hm_web_ui_doku/hm_devices_Endkunden.pdf
