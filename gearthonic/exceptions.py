"""All exceptions."""


class ProtocolUnknown(Exception):
    pass


class ConfigurationError(Exception):
    pass
