"""Fixtures for the pytest unittests."""

import pytest

from gearthonic import GearClient


@pytest.fixture
def gear_client():
    return GearClient('host', 1234)
